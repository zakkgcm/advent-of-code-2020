use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashMap;

#[aoc_generator(day10)]
fn read_input(input: &str) -> Vec<u32> {
    let mut sorted_input: Vec<u32> = input.lines().map(|x| x.parse::<u32>().unwrap()).collect();

    sorted_input.sort();
    sorted_input.insert(0, 0);
    sorted_input.push(sorted_input.last().unwrap() + 3);

    sorted_input
}

#[aoc(day10, part1)]
fn part1(input: &[u32]) -> u32 {
    let jolts = input
        .windows(2)
        .fold((0, 0, 0), |acc, x| match x[1] - x[0] {
            1 => (acc.0 + 1, acc.1, acc.2),
            2 => (acc.0, acc.1 + 1, acc.2),
            3 => (acc.0, acc.1, acc.2 + 1),
            _ => acc,
        });

    jolts.0 * jolts.2
}

#[aoc(day10, part2)]
fn part2(input: &[u32]) -> u64 {
    let mut memo: HashMap<usize, u64> = HashMap::new();
    memo.insert(0, 1);

    input.iter().enumerate().skip(1).for_each(|(i, x)| {
        let inpaths = (0..i).rev().fold(0, |acc, j| {
            if input[j] + 3 >= *x {
                acc + memo.get(&j).unwrap()
            } else {
                acc
            }
        });

        memo.insert(i, inpaths);
    });

    *memo.get(&(input.len() - 1)).unwrap()
}
