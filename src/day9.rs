use aoc_runner_derive::{aoc, aoc_generator};

use std::collections::HashMap;

const PREAMBLE_LEN: usize = 25;

#[aoc_generator(day9)]
fn get_xmas(input: &str) -> Vec<u64> {
    input.lines().map(|x| x.parse::<u64>().unwrap()).collect()
}

fn two_sum(haystack: &[u64], needle: &u64) -> Option<(u64, u64)> {
    let mut complements: HashMap<u64, u64> = HashMap::new();

    for x in haystack.iter() {
        // the sum of the parts is greater than the whole
        // but if one of the parts is bigger already then y'all dun goofed
        if needle > x {
            complements.insert(needle - x, *x);
        }

        if complements.contains_key(x) {
            return Some((*x, *complements.get(x).unwrap()));
        }
    }

    return None;
}

fn is_valid_number(preamble: &[u64], x: &u64) -> bool {
    two_sum(preamble, x).is_some()
}

#[aoc(day9, part1)]
fn part1(inputs: &[u64]) -> u64 {
    inputs[PREAMBLE_LEN + 1..]
        .windows(PREAMBLE_LEN + 1)
        .filter(|x| !is_valid_number(&x[..PREAMBLE_LEN], &x[PREAMBLE_LEN]))
        .next()
        .unwrap()[PREAMBLE_LEN]
}

#[aoc(day9, part2)]
fn part2(inputs: &[u64]) -> u64 {
    let canary = part1(inputs);

    // naive solution:
    // starting at 0, grow a window until the sum exceeds the canary
    // then shift the window by 1 and repeat
    // can improve by being clever about the partial sums

    let (start, size, _) = (0..inputs.len())
        .map(|i| {
            inputs[i..].iter().try_fold((i, 0, 0), |acc, x| {
                if acc.2 < canary {
                    Some((acc.0, acc.1 + 1, acc.2 + x))
                } else if acc.2 == canary {
                    Some((acc.0, acc.1, acc.2))
                } else {
                    None
                }
            })
        })
        .filter(|x| x.is_some())
        .next()
        .unwrap()
        .unwrap();

    let exploit_window = &inputs[start..(start + size)];

    exploit_window.iter().max().unwrap() + exploit_window.iter().min().unwrap()
}
