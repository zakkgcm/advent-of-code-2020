use aoc_runner_derive::aoc_lib;

pub mod day10;
pub mod day9;

aoc_lib! {year = 2020}
