use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use std::collections::HashMap;

type Edge = (i32, String);

fn parse_entry(entry: String) -> (String, Option<Vec<Edge>>) {
    let entry_spl: Vec<&str> = entry.splitn(2, "contain").collect();
    let bag_name = entry_spl[0].trim().strip_suffix(" bags").unwrap().to_string();

    let edge_entries: Vec<_> = entry_spl[1].split(",").collect();
    let mut edge_list: Vec<Edge> = Vec::new();

    if entry_spl[1] == " no other bags." {
        return (bag_name, None)
    }

    for edge in edge_entries.iter() {
        let keys = edge.split_whitespace().collect::<Vec<_>>();
        let (k, bag_type_unfiltered) = keys.split_first().unwrap();
        let (_, bag_type_tokens) = bag_type_unfiltered.split_last().unwrap();

        let bag_type = bag_type_tokens.join(" ");

        edge_list.push((k.parse::<i32>().unwrap(), bag_type));
    }

    (bag_name, Some(edge_list))
}


fn read_input<P>(fname:P) -> io::Result<HashMap<String, Option<Vec<Edge>>>>
where P: AsRef<Path>, {
    let file = File::open(fname)?;

    let lines = io::BufReader::new(file).lines();
    
    Ok(lines.map(|x| x.unwrap().to_owned()).map(parse_entry).collect::<HashMap::<_, _>>())
}

fn main() {
    //println!("{:?}", read_input("input"));
    
    let bag_graph = read_input("input").unwrap();

    // part 1
    let search_key = "shiny gold";
    let mut bag_count = 0;

    for bag in bag_graph.keys() {
        let mut bag_stack: Vec<String> = vec![bag.to_string()];
        let mut visited: Vec<String> = vec![];

        while !bag_stack.is_empty() {
            let bag_node = bag_stack.pop().unwrap();

            if !visited.contains(&bag_node) {
                visited.push(bag_node.clone());

                let bag_edges = bag_graph.get(&bag_node).unwrap();

                if bag_edges.is_none() {
                    continue;
                } else {
                    let (_, bag_edge_nodes) : (Vec<i32>, Vec<String>) = bag_edges.as_ref().unwrap().iter().cloned().unzip();
                    if bag_edge_nodes.contains(&search_key.to_string()) {
                        println!("Contains: {}", bag);
                        bag_count += 1;
                        break;
                    }

                    bag_edge_nodes.iter().for_each(|x| bag_stack.push(x.to_string()));
                }
            }
        }
    }

    println!("{}", bag_count);

    let mut shiny_bag_count = 0;

    let mut shiny_edge_stack: Vec<_> = vec![search_key.to_string()];
    let mut shiny_count_stack: Vec<_> = vec![1];

    while !shiny_edge_stack.is_empty() {
        //println!("{:?}", shiny_edge_stack);
        //println!("{}, {:?}", shiny_bag_count, shiny_count_stack);
        let bag_node = shiny_edge_stack.pop().unwrap();
        let bag_stack_count = shiny_count_stack.pop().unwrap();

        let bag_edges = bag_graph.get(&bag_node).unwrap();

        shiny_bag_count += bag_stack_count;
        if bag_edges.is_none() {
            // reached leaf edge, finished bag nesting
            // println!("leaf! {:?}", bag_stack_count);
            continue;

        } else {
            bag_edges.as_ref().unwrap().iter().cloned().for_each(|x| {
                shiny_edge_stack.push(x.1)
            });
            bag_edges.as_ref().unwrap().iter().cloned().for_each(|x| {
                shiny_count_stack.push(x.0 * bag_stack_count);
            });
        }
    }

    println!("{}", shiny_bag_count-1);
}
