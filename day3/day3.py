#!/usr/bin/env python3
import itertools

map_key = {'.': 0, '#': 1}

def read_input():
    the_map = []
    with open('input', 'r') as f:
        for line in f.readlines():
            the_map.append([map_key[x] for x in line.strip()])
        
    return the_map


def count_trees(the_map, dx = 3, dy = 1):
    x = 0
    y = 0

    width = len(the_map[0])
    height = len(the_map)

    trees = 0

    while y <= height-1:
        if the_map[y][x % width] == 1:
            trees = trees + 1

        x = x + dx
        y = y + dy

    return trees


if __name__ == "__main__":
    the_map = read_input()
    
    # part 1
    print(count_trees(the_map))

    # part 2
    print(count_trees(the_map))
    slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]

    tree_product = 1
    for slope in slopes:
        tree_product = tree_product * count_trees(the_map, slope[0], slope[1])

    print(tree_product)

    
    

