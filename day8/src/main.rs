use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use std::collections::HashMap;

type Instruction = (String, i32);

fn parse_entry(entry: String) -> Instruction {

    
    let entry_split: Vec<_>= entry.split_whitespace().collect();

    (entry_split[0].to_string(), entry_split[1].parse::<i32>().unwrap())
}


fn read_input<P>(fname:P) -> io::Result<Vec<Instruction>>
where P: AsRef<Path>, {
    let file = File::open(fname)?;

    let lines = io::BufReader::new(file).lines();
    
    Ok(lines.map(|x| x.unwrap().to_owned()).map(parse_entry).collect::<Vec<_>>())
}

fn run_program(instructions: &Vec<Instruction>) {
    let mut acc = 0;
    let mut pc: i32 = 0;

    let mut visited = vec![false; instructions.len()];

    loop {
        visited[pc as usize] = true;

        let inst = &instructions[pc as usize];

        println!("{} {}", inst.0, inst.1);

        match inst.0.as_str() {
            "acc" => { acc += inst.1; pc += 1; },
            "jmp" => pc += inst.1,
            "nop" => pc += 1,
            _ => ()
        }

        if visited[pc as usize] {
            println!("INFINITE LOOP DETECTED! HALTING PROBLEM SOLVED!!! {}", acc);
            break;
        }
    }
}

fn process_instruction(inst: &Instruction, pc: &mut i32, acc: &mut i32) {
    match inst.0.as_str() {
        "acc" => { *acc += inst.1; *pc += 1; },
        "jmp" => *pc += inst.1,
        "nop" => *pc += 1,
        _ => ()
    }
}

fn check_cycle(instructions: &Vec<Instruction>) -> bool {
    let mut acc_tort = 0;
    let mut acc_hare = 0;
    let mut pc_tort: i32 = 0;
    let mut pc_hare: i32 = 0;

    while (pc_tort as usize) < instructions.len() {
        let inst_tort = &instructions[pc_tort as usize];
        process_instruction(inst_tort, &mut pc_tort, &mut acc_tort);

        // FIXME: make these a function with a Result
        if (pc_hare as usize) < instructions.len() {
            let inst_hare = &instructions[pc_hare as usize];
            process_instruction(inst_hare, &mut pc_hare, &mut acc_hare);
        }

        if (pc_hare as usize) < instructions.len() {
            let inst_hare_2 = &instructions[pc_hare as usize];
            process_instruction(inst_hare_2, &mut pc_hare, &mut acc_hare);
        }

        //println!("{} {}", inst_tort.0, inst_tort.1);

        if (pc_hare as usize) < instructions.len() && pc_tort == pc_hare {
            //println!("cycle detected");
            return true
        }
    }

    println!("Program Finished! {}", acc_tort);
    false
}


fn fix_program(instructions: &Vec<Instruction>) {
    // FIXME: bruteforce bad
    // check every instruction for jmp and nop
    // swap the instruction and check if that program still has a cycle 
    for (i, inst) in instructions.iter().enumerate() {
        match inst.0.as_str() {
            "jmp" | "nop" => {
                let new_inst = (
                                    match inst.0.as_str() {
                                        "jmp" => "nop", "nop" => "jmp", _ => ""
                                    }.to_string(),
                                    inst.1
                                );

                let mut new_pgrm = instructions.clone();
                new_pgrm[i] = new_inst;


                check_cycle(&new_pgrm);
            },                
            _ => () 
        }
    }
}



fn main() {
    let program = read_input("input").unwrap();

    run_program(&program);
    fix_program(&program);
}
