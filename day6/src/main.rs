use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use std::collections::HashSet;

fn read_input<P>(fname:P) -> io::Result<Vec::<Vec::<HashSet::<char>>>>
where P: AsRef<Path>, {
    let mut ret = Vec::<Vec::<HashSet::<char>>>::new();

    let file = File::open(fname)?;

    let mut lines = io::BufReader::new(file).lines().peekable();

    // FIXME: this might break if the last group has only one person
    //        i.e. peek() is EOF but the current line is the start of a new group
    while lines.peek().is_some() {
        let group = lines.by_ref()
                         .take_while(|x| x.as_ref().unwrap_or(&"".to_string()) != "")
                         .map(|s| s.unwrap().chars().collect::<HashSet::<char>>())
                         .collect::<Vec::<HashSet::<char>>>();

        ret.push(group);
    }

    Ok(ret)
}

fn main() {
    let answers = read_input("input").unwrap();

    // intersect/union folds borrowed from https://github.com/rust-lang/rfcs/issues/2023
    println!("Part 1: {}", answers.iter().map(|group| {
                              group.iter()
                                   .fold(HashSet::new(), |acc, hs| {
                                         acc.union(hs).cloned().collect() })
                          }).collect::<Vec<HashSet::<char>>>()
                          .iter()
                          .fold(0, |acc, x| acc + x.len()));

    println!("Part 2: {:?}", answers.iter().map(|group| {
                                group.iter()
                                     .fold(group[0].clone(), |acc, hs| {
                                           acc.intersection(hs).cloned().collect() })
                          }).collect::<Vec<HashSet::<char>>>()
                          .iter()
                          .fold(0, |acc, x| acc + x.len()));
}
