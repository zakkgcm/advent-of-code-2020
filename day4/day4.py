#!/usr/bin/env python3
import itertools
import re

def read_input():
    passports = []
    with open('input', 'r') as f:
        passport = {}
        for line in f:
            if line.strip() == '':
                passports.append(passport)
                passport = {}
                continue

            entries = [entry.split(':') for entry in line.strip().split()]

            for k,v in entries:
                passport[k] = v

        # FIXME: theres a better way to do this
        passports.append(passport)
        passport = {}

    return passports

def validate_passport(passport, validate_fields=False):
    field_validation={'byr': lambda x: 1920 <= int(x) <= 2002,
                      'iyr': lambda x: 2010 <= int(x) <= 2020,
                      'eyr': lambda x: 2020 <= int(x) <= 2030,
                      'hgt': lambda x: (x[-2:] == 'cm' and 150 <= int(x[:-2]) <= 193) or (x[-2:] == 'in' and 59 <= int(x[:-2]) <= 76),
                      'hcl': lambda x: re.match('^#[0-9a-f]{6}$', x) != None,
                      'ecl': lambda x: x in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'),
                      'pid': lambda x: x.isnumeric() and len(x) == 9}

    if set(field_validation.keys()) - set(passport.keys()) == set():
        if validate_fields:
            for k,v in passport.items():
                if k in field_validation and not field_validation[k](v):
                    return False

        return True

    return False
    

if __name__ == "__main__":
    passports = read_input()
    
    # part 1
    print(sum(validate_passport(p) for p in passports))

    # part 2
    print(sum(validate_passport(p, True) for p in passports))
