#!/usr/bin/env python3
import itertools

def read_input():
    with open('input', 'r') as f:
        numbers = list(map(int, f.readlines()))

    return numbers

def two_sum(numbers, target = 2020):
    complements = {}

    for i in numbers:
        if i in complements:
            return i * complements[i]
        else:
            complements[target - i] = i

def three_sum(numbers, target = 2020):
    three_sum = {}

    for i in numbers:
        three_sum[target - i] = i

    for k in three_sum.keys():
        key_sum = {}

        two_sum_key = two_sum(numbers, k)
        if two_sum_key != None:
            return two_sum_key * three_sum[k]


if __name__ == "__main__":
    numbers = read_input()
    print(two_sum(numbers))
    print(three_sum(numbers))

