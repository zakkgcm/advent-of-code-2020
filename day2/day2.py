#!/usr/bin/env python3
import itertools

def read_input():
    db = []
    with open('input', 'r') as f:
        for line in f.readlines():
            policy, password = line.strip().split(':')
            letter_range, letter = policy.split(' ')
            min_letter, max_letter = map(int, letter_range.split('-'))

            db.append((min_letter, max_letter, letter, password))
        
    return db

def validate_password(policy, password):
    min_l, max_l, letter = policy
    
    count = 0
    for l in password:
        if l == letter:
            count = count + 1

    if min_l <= count <= max_l:
        return True
    
    return False
    
def validate_password_two(policy, password):
    pos_one, pos_two, letter = policy

    return (password[pos_one] == letter) ^ (password[pos_two] == letter)
    

def validate_db(db, validation_func=validate_password):
    valid = []
        
    for entry in db:
        valid.append(validation_func(entry[:3], entry[3]))

    return valid
        

if __name__ == "__main__":
    db = read_input()
    valid = validate_db(db)

    print (sum(valid))

    valid = validate_db(db, validate_password_two)

    print (sum(valid))
    
    

