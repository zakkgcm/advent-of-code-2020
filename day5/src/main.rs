use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn read_input<P>(fname:P) -> io::Result<Vec<String>>
where P: AsRef<Path>, {
    let mut ret = Vec::<String>::new();

    let file = File::open(fname)?;

    for line in io::BufReader::new(file).lines() {
        if let Ok(ip) = line {
            ret.push(ip)
        }
    }

    Ok(ret)
}

fn compute_seat_id(boarding_pass:&String) -> i32 {
    let mut row = 0b0000000;
    for i in boarding_pass[..7].chars() {
        row = match i {
            'F' => (row << 1) | 0,
            'B' => (row << 1) | 1,
             _  => 0,
        }
    }

    let mut col = 0b000;
    for i in boarding_pass[7..].chars() {
        col = match i {
            'L' => (col << 1) | 0,
            'R' => (col << 1) | 1,
            _   => 0,
        }
    }

    //println!("{}, {}, {}", boarding_pass, row, col);

    row * 8 + col
}

fn main() {
    let inputs = match read_input(Path::new("input")) {
        Ok(inputs)  => inputs,
        Err(e)      => panic!("Error reading inputs: {}!", e),
    };

    let mut seat_ids: Vec<i32> = inputs.iter().map(|x| compute_seat_id(x)).collect();
    seat_ids.sort();

    println!("The biggest seat ID is: {}", seat_ids.iter().max().unwrap());

    for (i, v) in seat_ids.iter().enumerate() {
        if i < seat_ids.len()-1 && seat_ids[i+1] != v+1 {
            println!("Your seat is: {}", v+1);
            break;
        }
    }
}
